# -*- coding: utf-8 -*-

# USAGE
# python color_kmeans.py --image images/jp.png --clusters 3

# import the necessary packages
from sklearn.cluster import KMeans
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
matplotlib.rcParams['font.family'] = 'TUM Neue Helvetica'
matplotlib.rcParams['font.size'] = 25
import matplotlib.pyplot as plt
import argparse
import utils
import cv2
import numpy as np

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
ap.add_argument("-c", "--clusters", required = True, type = int,help = "# of clusters")
args = vars(ap.parse_args())
(titel,ending) = args["image"].split('.')
print ("Start analyse "+titel+"...")

# load the image and convert it from BGR to RGB so that
# we can dispaly it with matplotlib
print ("Stard loading image...")
image = cv2.imread("input/"+args["image"])
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
print ("\tfinished.")

# reshape the image to be a list of pixels
print ("Start reshaping image...")
image = image.reshape((image.shape[0] * image.shape[1], 3))
print ("\tfinished.")

print ("Start applying KMeans...")
# cluster the pixel intensities
clt = KMeans(n_clusters = args["clusters"])
print ("\t...fit...")
clt.fit(image)
print ("\tfinished.")

# build a histogram of clusters and then create a figure
# representing the number of pixels labeled to each color
print ("Start generating histogram...")
hist = utils.centroid_histogram(clt)
print ("\t...plot_colors...")
bar = utils.plot_colors(hist, clt.cluster_centers_)
#print ("\t...imshow...")
#im = plt.imshow(bar)
##im.set_title("Distribucion: "+titel)
#print ("\t...savefig...")
#im.figure.savefig("output/hist_"+titel+".png",dpi=200)
print ("\tfinished.")

print ("Start generating pie chart...")
fig,ax,pie_collection = utils.plot_piechart(hist, clt.cluster_centers_)
ax.set_title("Distribucion: "+titel,fontweight="bold")
print ("\t...savefig...")
fig.savefig("output/pie_"+titel+".png",dpi=200)
print ("\tfinished.")

print ("End application.\n")
