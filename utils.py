# -*- coding: utf-8 -*-

# import the necessary packages
import numpy as np
import cv2
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['font.family'] = 'TUM Neue Helvetica'
matplotlib.rcParams['font.size'] =  25
import matplotlib.pyplot as plt

def centroid_histogram(clt):
    # grab the number of different clusters and create a histogram
    # based on the number of pixels assigned to each cluster
    numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
    (hist, _) = np.histogram(clt.labels_, bins = numLabels)

    # normalize the histogram, such that it sums to one
    hist = hist.astype("float")
    hist /= hist.sum()

    # return the histogram
    return hist

def plot_colors(hist, centroids):
    # initialize the bar chart representing the relative frequency
    # of each of the colors
    height = 50
    length = 300
    bar = np.zeros((height, length, 3), dtype = "uint8")
    startX = 0

    # loop over the percentage of each cluster and the color of
    # each cluster
    percent_white = 0
    percent_black = 0
    for (percent, color) in zip(hist, centroids):
        colors = color.astype("uint8").tolist()
        #print (colors)

        if (colors[0]>245 and colors[1]>245 and colors[2]>245):
            percent_white = percent

        if (colors[0]<10 and colors[1]<10 and colors[2]<10):
            percent_black = percent

    #print (percent_white)
    #print (percent_black)

    for (percent, color) in zip(hist, centroids):
        # plot the relative percentage of each cluster
        colors = color.astype("uint8").tolist()

        if ((colors[0]<245 and colors[1]<245 and colors[2]<245) or (colors[0]>10 and colors[1]>10 and colors[2]>10)):
            endX = startX + (percent/(1-(percent_white+percent_black)) * length)
            cv2.rectangle(bar, (int(startX), 0), (int(endX), height),
                colors, -1)
            startX = endX

    # return the bar chart
    return bar

def plot_piechart(hist, centroids):
    label_colors = {
                    'white'         :   [255,255,255],
                    'black'         :   [0,0,0],
                    'Residencial'   :   [161,206,246],
                    'Hospendaje'    :   [83,111,225],
                    'Comercio'      :   [248,153,52],
                    'Servicio'      :   [251,230,65],
                    'Oficina'       :   [235,40,40],
                    'Educacion'     :   [229,140,197],
                    'Cultura'       :   [169,62,182],
                    'Gasolinera'    :   [128,160,84]
                   }
    label_tupel = ('white','black','Residencial','Hospendaje','Comercio','Servicio','Oficina','Educacion','Cultura','Gasolinera')
    colordict = {}
    values = []
    labels = []
    explodes = []
    ratio_white = 0
    ratio_black = 0

    threshold = 50
    for label in label_tupel:
        print ("    searching for label: {}...".format(label))
        color = label_colors[label]
        for (center,value) in zip(centroids,hist):
            cen = [int(i) for i in center]
            if ((cen[0]>(color[0]-threshold) and cen[0]<(color[0]+threshold)) and (cen[1]>(color[1]-threshold) and cen[1]<(color[1]+threshold)) and (cen[2]>(color[2]-threshold) and cen[2]<(color[2]+threshold))):
                if (label == 'white' or label == 'black'):
                    if ( label == 'white'):
                        ratio_white = value
                        print ("\tcen: {}".format(cen))
                        print ("\t--> match with value: {} ({})".format(value,color))
                        break
                    else:
                        ratio_black = value
                        print ("\tcen: {}".format(cen))
                        print ("\t--> match with value: {} ({})".format(value,color))
                        break
                else:
                    colordict[label] = [i/255 for i in color]
                    values.append(value)
                    labels.append(label)
                    explodes.append(0)
                    #if (label == 'Comercio'):
                    #    explodes.append(0.1)
                    #else:
                    #    explodes.append(0)
                    print ("\tcen: {}".format(cen))
                    print ("\t--> match with value: {} ({})".format(value,color))
                    break
            #else:
                #print ("\t--> no match.")

    values = [i/(1-(ratio_black+ratio_white)) for i in values]
    #print ("len(values): {}".format(len(values)))
    #print ("len(labels): {}".format(len(labels)))
    #print ("\ncentroids: {}\ncolordict: {}".format(len(centroids),len(colordict)))

    #explodes = np.zeros(len(values))
    #print ("len(explodes): {}".format(len(explodes)))
    #explodes[1] = 0.1

    fig = plt.figure(figsize=[12,12])
    ax = fig.add_subplot(111)

    pie_collection = ax.pie(values, explode=explodes, labels=labels, autopct='%1.1f%%',shadow=False, startangle=0)

    for pie_wedge in pie_collection[0]:
        pie_wedge.set_facecolor(colordict[pie_wedge.get_label()])
        pie_wedge.set_edgecolor('white')

    return fig,ax,pie_collection
